<?php

use App\Services\Application;
use Symfony\Component\HttpFoundation\Request;

chdir(__DIR__ . '/..');
require 'vendor/autoload.php';

/* @var $container \Pimple\Psr11\Container */
$container = require 'src/container.php';

/* @var $app Application */
$app = $container->get(Application::class);
$request = $container->get(Request::class);
$response = $app->handle($request);
$response->send();
$app->terminate($request, $response);
