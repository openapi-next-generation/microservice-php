<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('vendor')
    ->in(__DIR__)
;

return PhpCsFixer\Config::create()
    ->setRules([
        '@PSR2' => true,
        'class_attributes_separation' => ['elements' => ['method']]
    ])
    ->setFinder($finder)
;