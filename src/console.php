<?php
/* @var $container \Pimple\Psr11\Container */
/* @var $app \Symfony\Component\Console\Application */

use OpenapiNextGeneration\ApiDocsGeneratorPhp\ApiDocsGenerator;
use OpenapiNextGeneration\EntityGeneratorPhp\EntityBuilder;
use OpenapiNextGeneration\JsonSchemaGeneratorPhp\JsonSchemaBuilder;
use OpenapiNextGeneration\MicroframeworkToolsPhp\Generation\OpenapiGeneratorProvider;
use OpenapiNextGeneration\MicroframeworkToolsPhp\Generation\RoutesUpdater;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\PatternMapper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

$app->register('generate-all')
    ->setDescription('Run all generators')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($container) {
        $specification = $container->get(OpenapiGeneratorProvider::API_SPECIFICATION);

        /* @var $patternMapper PatternMapper */
        $patternMapper = $container->get(PatternMapper::class);
        $patterns = $patternMapper->buildPatterns($specification);

        $output->writeln('building docs...');
        /* @var $apiDocsGenerator ApiDocsGenerator */
        $apiDocsGenerator = $container->get(ApiDocsGenerator::class);
        $apiDocsGenerator->buildHtml($specification, 'docs/index.html');

        $output->writeln('building json-schema files...');
        /* @var $jsonSchemaBuilder JsonSchemaBuilder */
        $jsonSchemaBuilder = $container->get(JsonSchemaBuilder::class);
        $jsonSchemaBuilder->buildSchemas($specification, 'docs/json-schema');

        $output->writeln('building api entities...');
        /* @var $entityBuilder EntityBuilder */
        $entityBuilder = $container->get(EntityBuilder::class);
        $entityBuilder->buildEntities($patterns, 'App\\Api', 'src/Api');

        $output->writeln('building routes and controllers...');
        /* @var $routesUpdater RoutesUpdater */
        $routesUpdater = $container->get(RoutesUpdater::class);
        $routesUpdater->updateRoutesFile($specification, 'src/routes.php');
        $routesUpdater->updateControllers($specification, 'App\\Controllers', 'src/Controllers');

        $output->writeln('fixing code style...');
        $phpCsFixer = new \PhpCsFixer\Console\Application();
        /* @var $fixCommand \PhpCsFixer\Console\Command\FixCommand */
        $fixCommand = $phpCsFixer->get('fix');
        $fixCommand->run($input, $output);
    });
