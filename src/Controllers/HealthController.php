<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HealthController
{
    public function get(Request $request, Container $container)
    {
        return new Response('I am alive');
    }
}
