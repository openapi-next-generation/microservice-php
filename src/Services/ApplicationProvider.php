<?php

namespace App\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ApplicationProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple[Application::class] = function (Container $pimple) {
            return new Application($pimple['config'], new \Pimple\Psr11\Container($pimple));
        };
    }
}
