<?php

namespace App\Services;

use JsonSchema\Validator;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Application extends \OpenapiNextGeneration\MicroframeworkToolsPhp\Application
{
    public function terminate(Request $request, Response $response)
    {
    }

    protected function onRouteMatched(Request $request)
    {
        $this->validateRequestBody($request);
    }

    protected function validateRequestBody(Request $request)
    {
        $route = new Route('', $request->getPathInfo(), []);
        $validationFile = 'docs/json-schema' . $route->getPathName() . '/' . strtolower($request->getMethod()) . '/request/schema.json';
        if (is_file($validationFile)) {
            $validator = new Validator();
            $validator->validate(json_decode($request->getContent()), ['$ref' => 'file://' . realpath($validationFile)]);
            if (!$validator->isValid()) {
                throw new \Exception(json_encode($validator->getErrors()));
            }
        }
    }
}
