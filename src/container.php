<?php

use App\Services\ApplicationProvider;
use OpenapiNextGeneration\MicroframeworkToolsPhp\ConfigProvider;
use OpenapiNextGeneration\MicroframeworkToolsPhp\ConsoleApplicationProvider;
use OpenapiNextGeneration\MicroframeworkToolsPhp\FastRouteDispatcherProvider;
use OpenapiNextGeneration\MicroframeworkToolsPhp\Generation\OpenapiGeneratorProvider;
use OpenapiNextGeneration\MicroframeworkToolsPhp\RequestProvider;
use Pimple\Psr11\Container;

$pimple = new \Pimple\Container();
$container = new Container($pimple);

$pimple->register(new RequestProvider());
$pimple->register(new ConfigProvider());
$pimple->register(new FastRouteDispatcherProvider());

$pimple->register(new ApplicationProvider());
$pimple->register(new ConsoleApplicationProvider());
$pimple->register(new OpenapiGeneratorProvider());

return $container;
