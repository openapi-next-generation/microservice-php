<?php
/* @var $routes \FastRoute\RouteCollector */

use App\Controllers\HealthController;

$routes->addRoute('GET', '/health', HealthController::class . ':get');
